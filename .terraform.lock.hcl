# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version = "4.22.0"
  hashes = [
    "h1:RxPzK6VFHz6qZMZUVhE03j9Cf5CvnLr14egtq5yxD1E=",
    "zh:299efb8ba733b7742f0ef1c5c5467819e0c7bf46264f5f36ba6b6674304a5244",
    "zh:4db198a41d248491204d4ca644662c32f748177d5cbe01f3c7adbb957d4d77f0",
    "zh:62ebc2b05b25eafecb1a75f19d6fc5551faf521ada9df9e5682440d927f642e1",
    "zh:636b590840095b4f817c176034cf649f543c0ce514dc051d6d0994f0a05c53ef",
    "zh:8594bd8d442288873eee56c0b4535cbdf02cacfcf8f6ddcf8cd5f45bb1d3bc80",
    "zh:8e18a370949799f20ba967eec07a84aaedf95b3ee5006fe5af6eae13fbf39dc3",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:aa968514231e404fb53311d8eae2e8b6bde1fdad1f4dd5a592ab93d9cbf11af4",
    "zh:af8e5c48bf36d4fff1a6fca760d5b85f14d657cbdf95e9cd5e898c68104bad31",
    "zh:d8a75ba36bf8b6f2e49be5682f48eccb6c667a4484afd676ae347213ae208622",
    "zh:dd7c419674a47e587dabe98b150a8f1f7e31c248c68e8bf5e9ca0a400b5e2c4e",
    "zh:fdeb6314a2ce97489bbbece59511f78306955e8a23b02cbd1485bd04185a3673",
  ]
}

provider "registry.terraform.io/linode/linode" {
  version     = "1.28.1"
  constraints = "1.28.1"
  hashes = [
    "h1:eRVL+bDk2k6Y7Ig+VyZUU1jMdS9eiepK4PV55pMbvoQ=",
    "zh:1a3819dd7e6408edebb855037acba53e1ef35e9e37ac69983b028332f096965b",
    "zh:2ca6b21ff5d1afe0482517e2a4330b0f358820c03a40e1ff84b7a95b84c6bf11",
    "zh:6212deb0ddb95214c8ed95867c5aca2f3db57192bab6a20ff925552ad813e860",
    "zh:66f686088406e5006e59efc9858d8ac08ad1456ee8de5e4ea0ede7f50018cf93",
    "zh:8f5335a71fb5bb876f6c6c8523abfe4d369e4d831d8e2501c7bc695a4f35f873",
    "zh:955c4372648aa1415f8d3b14ddfa0b0695210d211aa29e4d665ee45721763095",
    "zh:b798f5fefd0c71f8bc411ea9af3a58af9d80c78cd4d2e88e6bd07b68c62ddde3",
    "zh:bc08690d655ba74f72bdc8a9bf45ac45e97b334b9b08713a9b85dbb4e6e898e7",
    "zh:c344e93349d79c5dc6ced60da688b5d5ba14c2e3636aba70d509128e76e155fe",
    "zh:d249b154b5d87741880da790bbdf38ed1fbaa9b7cf4699d3496f4c475a3e68ae",
    "zh:e03210f86937adfff080dba4b2f84dad0e1f7b1dbee98df805c2c8d9fa4dd1e9",
    "zh:e84d6438ba1db954ebd1e66e0d21fc7097eb63253d53b35ba9c0db571d8684ea",
    "zh:e9bea27bd515b86973222d1d9c2bb8108df9c49003c8b2a14b91dbcc70cd5f80",
    "zh:facb3933992a6eb3a421d2544d5528e46619f7d0efef7144a142b809f877101f",
  ]
}
