terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.22.0"
    }
    
    linode = {
      source = "linode/linode"
      version = "1.28.1"
    }
  }
}
